Orthanc PHP client
------------------

This project aims to provide an high level client to use Orthanc HTTP API. It
uses the Orthanc OpenAPI definition available at: https://api.orthanc-server.com/

The base interaction layer is generated using [Jane PHP](https://github.com/janephp/janephp).
It's based on the HTTP PSRs to be easily integrated with any PHP project.

Then high level objects are designed to interact with the API in an easy way.
